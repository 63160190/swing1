/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.swing1;

import java.awt.Color;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JTextField;

class MyActionListener implements ActionListener{

    @Override
    public void actionPerformed(ActionEvent e) {
        System.out.println("MyActionListener:Action");
    }
    
}
/**
 *
 * @author User
 */
public class HelloMe implements ActionListener{
    public static void main(String[] args) {
        JFrame frmMain = new JFrame("Hello Me");
        frmMain.setSize(500, 300);
        frmMain.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
         
        JLabel lblYourName=new JLabel("Your Name:");
        lblYourName.setSize(80, 20);
        lblYourName.setLocation(5, 5);
        lblYourName.setBackground(Color.WHITE);
        lblYourName.setOpaque(true);
        
        JTextField txtYourName=new JTextField();
        txtYourName.setSize(200,20);
        txtYourName.setLocation(90,5);
        
        JButton bthHello = new JButton("Hello");
        bthHello.setSize(80, 20);
        bthHello.setLocation(90, 40);
        
        MyActionListener myActionListener=new MyActionListener();
        bthHello.addActionListener(myActionListener);
        bthHello.addActionListener(new HelloMe());
        
        ActionListener actionListener = new ActionListener(){
            @Override
            public void actionPerformed(ActionEvent e) {
                System.out.println("Anmnymous Class:Action");
            }
        };
        bthHello.addActionListener(actionListener);
        
        
        JLabel lblHello = new JLabel("Hello...");
        lblHello.setSize(200, 20);
        lblHello.setLocation(90, 70);
        
        frmMain.setLayout(null);
        frmMain.add(lblYourName);
        frmMain.add(txtYourName);
        frmMain.add(bthHello);
        frmMain.add(lblHello);
        
        bthHello.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
              String name = txtYourName.getText(); 
              lblHello.setText("Hello"+name);
            }
        });
        
        frmMain.setVisible(true);
    }

    @Override
    public void actionPerformed(ActionEvent e) {
        System.out.println("HelloMe:Action");
    }
}
